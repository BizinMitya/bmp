import employee.Employee;
import employee.EmployeeHome;

import javax.ejb.FinderException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Properties;

/**
 * Created by Dmitriy on 17.04.2016.
 */
public class Main {
    public static void main(String[] args) {
        try {
            Properties properties = new Properties();
            properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
            properties.put(InitialContext.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
            properties.put("jboss.naming.client.ejb.context", true);
            properties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");
            InitialContext context = new InitialContext(properties);
            EmployeeHome employeeHome = (EmployeeHome) context.lookup("/BMP_ear_exploded/ejb/EmployeeBean!employee.EmployeeHome");
            Employee employee = employeeHome.findByPrimaryKey(7698);
            System.out.println(employee.getEmpno() + " " + employee.getEname() + " " + employee.getJob() + " " +
                    employee.getMgr() + " " + employee.getHiredate() + " " + employee.getComm() + " " +
                    employee.getSal() + " " + employee.getDeptno());
            List<Employee> employees = (List<Employee>) employeeHome.findByEname("Dmitriy");
            for(Employee employee1:employees){
                System.out.println(employee1.getEmpno() + " " + employee1.getEname() + " " + employee1.getJob() + " " +
                        employee1.getMgr() + " " + employee1.getHiredate() + " " + employee1.getComm() + " " +
                        employee1.getSal() + " " + employee1.getDeptno());
            }
        } catch (NamingException | FinderException | RemoteException e) {
            e.printStackTrace();
        }
    }
}
