package employee;

import javax.ejb.EJBObject;
import java.rmi.RemoteException;

/**
 * Created by Dmitriy on 17.04.2016.
 */
public interface Employee extends EJBObject {
    Integer getEmpno() throws RemoteException;

    void setEmpno(Integer empno) throws RemoteException;

    String getEname() throws RemoteException;

    void setEname(String ename) throws RemoteException;

    String getJob() throws RemoteException;

    void setJob(String job) throws RemoteException;

    Integer getMgr() throws RemoteException;

    void setMgr(Integer mgr) throws RemoteException;

    String getHiredate() throws RemoteException;

    void setHiredate(String hiredate) throws RemoteException;

    Integer getSal() throws RemoteException;

    void setSal(Integer sal) throws RemoteException;

    Integer getComm() throws RemoteException;

    void setComm(Integer comm) throws RemoteException;

    Integer getDeptno() throws RemoteException;

    void setDeptno(Integer deptno) throws RemoteException;
}
